# tau-gep



## Tel Aviv phase 2 TAU trigger study

There are two parts in this git:

- [ ] NNtraining(running on Colab): code for the DNN training [DNN_Notebook.ipynb](https://gitlab.cern.ch/bochen/tau-gep/-/blob/master/NNtraining/DNN_Notebook.ipynb); example of 100 TOBs of background [BKG.csv](https://gitlab.cern.ch/bochen/tau-gep/-/blob/master/NNtraining/BKG.csv) and signal [Signal.csv](https://gitlab.cern.ch/bochen/tau-gep/-/blob/master/NNtraining/Signal.csv); output scores [Scores_Example.csv](https://gitlab.cern.ch/bochen/tau-gep/-/blob/master/NNtraining/Scores_Example.csv) and the code for the Turn On Curve(TOC) [Generate_TOC.ipynb](https://gitlab.cern.ch/bochen/tau-gep/-/blob/master/NNtraining/Generate_TOC.ipynb)
- [ ] hls4ml(with conda environment): notebook for the hls4ml [tau2_hls4ml.ipynb](https://gitlab.cern.ch/bochen/tau-gep/-/blob/master/hls4ml/tau2_hls4ml.ipynb)
